(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function getHero() {
  $.getJSON("https://aws.baghome.com.br/heros", function (data) {
    var items = [];
    $.each(data, function (key, val) {
      items.push("<div class='aspect-ratio'><div class='aspect-inside' ><img style='object-fit: contain; width: 100%;' src='" + val.photo + "'></div></div>");
    });

    $(".caroucelRoot").append(items);
  });
}
function getVideos() {
  $.getJSON("https://aws.baghome.com.br/videos", function (data) {
    var items = [];
    $.each(data, function (key, val) {
      items.push("<div class= 'col-12 col-sm-6 col-md-3' >" +
        "<div class='contentVideo'>" +
        "<a href='" + val.link + "' target='_blank'>" +
        "<div class='thumbvds'>" +
        "<img src='" + val.image + "'>" +
        "</div>" +
        "</a>" +
        "<a href='" + val.link + "'> " +
        "<div class='texter'>" + val.title + "</div> " +
        "</a> " +
        "</div>" +
        "</div>");
    });

    $(".multiple-items").append(items);
  });
}
function getTestmon() {
  $.getJSON("https://aws.baghome.com.br/testimonials", function (data) {
    var items = [];
    $.each(data, function (key, val) {
      items.push(
        "<div class=''>" +
        "<div class='contentrs'>" +
        "<p class='ballon'>" + val.text + "</p>" +
        "<div class='donwPart'>" +
        "<p class='name'>" + val.name + "</p>" +
        "</div>" +
        "</div>" +
        "</div>");
    });

    $(".testimonials").append(items);
  });
}
function hideLoad(){
  console.log("Call hideLoad");
  $("#loaderID").hide(1000);

  $('.caroucelRoot').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    focusOnSelect: true
  });
  $('.testimonials').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: true,
    focusOnSelect: true
  });
  $('.multiple-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    responsive: [{
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  });
}
getHero()
getVideos()
getTestmon()

$( document ).ready(function(){
  setTimeout(hideLoad, 2000);
  // setTimeout(runSlides(), 1000);
});

},{}]},{},[1])
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZWx2aW4vZGV2ZWxvcG1lbnQvYmFnaG9tZS9sYW5kcGFnZS9zdGF0aWMvbm9kZV9tb2R1bGVzL2Jyb3dzZXItcGFjay9fcHJlbHVkZS5qcyIsIi9Vc2Vycy9rZWx2aW4vZGV2ZWxvcG1lbnQvYmFnaG9tZS9sYW5kcGFnZS9zdGF0aWMvanMvc3JjL2Zha2VfODliNDBmYTcuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dGhyb3cgbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKX12YXIgZj1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwoZi5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxmLGYuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiZnVuY3Rpb24gZ2V0SGVybygpIHtcbiAgJC5nZXRKU09OKFwiaHR0cHM6Ly9hd3MuYmFnaG9tZS5jb20uYnIvaGVyb3NcIiwgZnVuY3Rpb24gKGRhdGEpIHtcbiAgICB2YXIgaXRlbXMgPSBbXTtcbiAgICAkLmVhY2goZGF0YSwgZnVuY3Rpb24gKGtleSwgdmFsKSB7XG4gICAgICBpdGVtcy5wdXNoKFwiPGRpdiBjbGFzcz0nYXNwZWN0LXJhdGlvJz48ZGl2IGNsYXNzPSdhc3BlY3QtaW5zaWRlJyA+PGltZyBzdHlsZT0nb2JqZWN0LWZpdDogY29udGFpbjsgd2lkdGg6IDEwMCU7JyBzcmM9J1wiICsgdmFsLnBob3RvICsgXCInPjwvZGl2PjwvZGl2PlwiKTtcbiAgICB9KTtcblxuICAgICQoXCIuY2Fyb3VjZWxSb290XCIpLmFwcGVuZChpdGVtcyk7XG4gIH0pO1xufVxuZnVuY3Rpb24gZ2V0VmlkZW9zKCkge1xuICAkLmdldEpTT04oXCJodHRwczovL2F3cy5iYWdob21lLmNvbS5ici92aWRlb3NcIiwgZnVuY3Rpb24gKGRhdGEpIHtcbiAgICB2YXIgaXRlbXMgPSBbXTtcbiAgICAkLmVhY2goZGF0YSwgZnVuY3Rpb24gKGtleSwgdmFsKSB7XG4gICAgICBpdGVtcy5wdXNoKFwiPGRpdiBjbGFzcz0gJ2NvbC0xMiBjb2wtc20tNiBjb2wtbWQtMycgPlwiICtcbiAgICAgICAgXCI8ZGl2IGNsYXNzPSdjb250ZW50VmlkZW8nPlwiICtcbiAgICAgICAgXCI8YSBocmVmPSdcIiArIHZhbC5saW5rICsgXCInIHRhcmdldD0nX2JsYW5rJz5cIiArXG4gICAgICAgIFwiPGRpdiBjbGFzcz0ndGh1bWJ2ZHMnPlwiICtcbiAgICAgICAgXCI8aW1nIHNyYz0nXCIgKyB2YWwuaW1hZ2UgKyBcIic+XCIgK1xuICAgICAgICBcIjwvZGl2PlwiICtcbiAgICAgICAgXCI8L2E+XCIgK1xuICAgICAgICBcIjxhIGhyZWY9J1wiICsgdmFsLmxpbmsgKyBcIic+IFwiICtcbiAgICAgICAgXCI8ZGl2IGNsYXNzPSd0ZXh0ZXInPlwiICsgdmFsLnRpdGxlICsgXCI8L2Rpdj4gXCIgK1xuICAgICAgICBcIjwvYT4gXCIgK1xuICAgICAgICBcIjwvZGl2PlwiICtcbiAgICAgICAgXCI8L2Rpdj5cIik7XG4gICAgfSk7XG5cbiAgICAkKFwiLm11bHRpcGxlLWl0ZW1zXCIpLmFwcGVuZChpdGVtcyk7XG4gIH0pO1xufVxuZnVuY3Rpb24gZ2V0VGVzdG1vbigpIHtcbiAgJC5nZXRKU09OKFwiaHR0cHM6Ly9hd3MuYmFnaG9tZS5jb20uYnIvdGVzdGltb25pYWxzXCIsIGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgdmFyIGl0ZW1zID0gW107XG4gICAgJC5lYWNoKGRhdGEsIGZ1bmN0aW9uIChrZXksIHZhbCkge1xuICAgICAgaXRlbXMucHVzaChcbiAgICAgICAgXCI8ZGl2IGNsYXNzPScnPlwiICtcbiAgICAgICAgXCI8ZGl2IGNsYXNzPSdjb250ZW50cnMnPlwiICtcbiAgICAgICAgXCI8cCBjbGFzcz0nYmFsbG9uJz5cIiArIHZhbC50ZXh0ICsgXCI8L3A+XCIgK1xuICAgICAgICBcIjxkaXYgY2xhc3M9J2RvbndQYXJ0Jz5cIiArXG4gICAgICAgIFwiPHAgY2xhc3M9J25hbWUnPlwiICsgdmFsLm5hbWUgKyBcIjwvcD5cIiArXG4gICAgICAgIFwiPC9kaXY+XCIgK1xuICAgICAgICBcIjwvZGl2PlwiICtcbiAgICAgICAgXCI8L2Rpdj5cIik7XG4gICAgfSk7XG5cbiAgICAkKFwiLnRlc3RpbW9uaWFsc1wiKS5hcHBlbmQoaXRlbXMpO1xuICB9KTtcbn1cbmZ1bmN0aW9uIGhpZGVMb2FkKCl7XG4gIGNvbnNvbGUubG9nKFwiQ2FsbCBoaWRlTG9hZFwiKTtcbiAgJChcIiNsb2FkZXJJRFwiKS5oaWRlKDEwMDApO1xuXG4gICQoJy5jYXJvdWNlbFJvb3QnKS5zbGljayh7XG4gICAgc2xpZGVzVG9TaG93OiAxLFxuICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgIGF1dG9wbGF5U3BlZWQ6IDIwMDAsXG4gICAgYXJyb3dzOiB0cnVlLFxuICAgIGZvY3VzT25TZWxlY3Q6IHRydWVcbiAgfSk7XG4gICQoJy50ZXN0aW1vbmlhbHMnKS5zbGljayh7XG4gICAgc2xpZGVzVG9TaG93OiAxLFxuICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgIGF1dG9wbGF5U3BlZWQ6IDIwMDAsXG4gICAgYXJyb3dzOiBmYWxzZSxcbiAgICBkb3RzOiB0cnVlLFxuICAgIGZvY3VzT25TZWxlY3Q6IHRydWVcbiAgfSk7XG4gICQoJy5tdWx0aXBsZS1pdGVtcycpLnNsaWNrKHtcbiAgICBpbmZpbml0ZTogdHJ1ZSxcbiAgICBzbGlkZXNUb1Nob3c6IDQsXG4gICAgc2xpZGVzVG9TY3JvbGw6IDIsXG4gICAgYXV0b3BsYXk6IHRydWUsXG4gICAgYXV0b3BsYXlTcGVlZDogMzAwMCxcbiAgICBhcnJvd3M6IHRydWUsXG4gICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgIGJyZWFrcG9pbnQ6IDUwMCxcbiAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDFcbiAgICAgIH1cbiAgICB9XG4gICAgXVxuICB9KTtcbn1cbmdldEhlcm8oKVxuZ2V0VmlkZW9zKClcbmdldFRlc3Rtb24oKVxuXG4kKCBkb2N1bWVudCApLnJlYWR5KGZ1bmN0aW9uKCl7XG4gIHNldFRpbWVvdXQoaGlkZUxvYWQsIDIwMDApO1xuICAvLyBzZXRUaW1lb3V0KHJ1blNsaWRlcygpLCAxMDAwKTtcbn0pO1xuIl19
