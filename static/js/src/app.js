function getHero() {
  $.getJSON("https://aws.baghome.com.br/heros", function (data) {
    var items = [];
    $.each(data, function (key, val) {
      items.push("<div class='aspect-ratio'><div class='aspect-inside' ><img style='object-fit: contain; width: 100%;' src='" + val.photo + "'></div></div>");
    });

    $(".caroucelRoot").append(items);
  });
}
function getVideos() {
  $.getJSON("https://aws.baghome.com.br/videos", function (data) {
    var items = [];
    $.each(data, function (key, val) {
      items.push("<div class= 'col-12 col-sm-6 col-md-3' >" +
        "<div class='contentVideo'>" +
        "<a href='" + val.link + "' target='_blank'>" +
        "<div class='thumbvds'>" +
        "<img src='" + val.image + "'>" +
        "</div>" +
        "</a>" +
        "<a href='" + val.link + "'> " +
        "<div class='texter'>" + val.title + "</div> " +
        "</a> " +
        "</div>" +
        "</div>");
    });

    $(".multiple-items").append(items);
  });
}
function getTestmon() {
  $.getJSON("https://aws.baghome.com.br/testimonials", function (data) {
    var items = [];
    $.each(data, function (key, val) {
      items.push(
        "<div class=''>" +
        "<div class='contentrs'>" +
        "<p class='ballon'>" + val.text + "</p>" +
        "<div class='donwPart'>" +
        "<p class='name'>" + val.name + "</p>" +
        "</div>" +
        "</div>" +
        "</div>");
    });

    $(".testimonials").append(items);
  });
}
function hideLoad(){
  console.log("Call hideLoad");
  $("#loaderID").hide(1000);

  $('.caroucelRoot').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    focusOnSelect: true
  });
  $('.testimonials').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: true,
    focusOnSelect: true
  });
  $('.multiple-items').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    responsive: [{
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  });
}
getHero()
getVideos()
getTestmon()

$( document ).ready(function(){
  setTimeout(hideLoad, 2000);
  // setTimeout(runSlides(), 1000);
});
